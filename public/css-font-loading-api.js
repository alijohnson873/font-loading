if ("fonts" in document) {
    var regular = new FontFace("Open Sans", "url(/fonts/OpenSans.woff2) format('woff2'), url(/fonts/OpenSans.woff) format('woff')", { style: 'normal', weight: 400 });
    var bold = new FontFace("Open Sans", "url(/fonts/OpenSans-Bold.woff2) format('woff2'), url(/fonts/OpenSans-Bold.woff) format('woff')", { style: 'normal', weight: 700 });

    console.info('Current status', regular.status);
    console.info('Current status', bold.status);

    Promise.all([bold.load(), regular.load()]).then(function (fonts) {

        console.info('Current status', regular.status);
        console.info('Current status', bold.status);


        fonts.forEach(function (font) {
            document.fonts.add(font);
        });
    });
}

// // Simple check if the browser understands the API
// // Is there a better/more reliable way of checking support?
// if (document.fonts) {
//     // Define a new FontFace
//     var regular = new FontFace("Open Sans", "url(/fonts/OpenSans.woff2) format('woff2'), url(/fonts/OpenSans.woff) format('woff')");
//     //console.log(regular);

//     // Add the FontFace to the FontFaceSet
//     document.fonts.add(regular);

//     // Get the current status of the FontFace
//     // (should be 'unloaded')
//     console.info('Current status', regular.status);

//     // Load the FontFace
//     regular.load();

//     // Get the current status of the Fontface
//     // (should be 'loading' or 'loaded' if cached)
//     console.info('Current status', regular.status);

//     // Wait until the font has been loaded, log the current status.
//     regular.loaded.then((fontFace) => {
//         console.info('Current status', fontFace.status);
//         console.log(fontFace.family, 'loaded successfully.');

//         // Add a class to the body element
//         document.body.classList.add('open-sans-loaded');

//         // Throw an error if loading wasn't successful
//     }, (fontFace) => {
//         console.error('Current status', regular.status);
//     });

//     // Track if all fonts (if there are multiple) have been loaded
//     // The 'ready' promise resolves if this is the case
//     document.fonts.ready.then((fontFaceSet) => {
//         console.log(fontFaceSet.size, 'FontFaces loaded.');
//     });

// } else {
//     console.error('Sorry, unsupported browser');
// }
